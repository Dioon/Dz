#include "mainwindow.h"
#include "ui_mainwindow.h"

int projectNumber;
QString nowProject;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle( " Govorun File Reader " );
    projectNumber = 1;
    nowProject = "";
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_action_2_triggered()
{
    // exit
    close();
}

void MainWindow::on_action_triggered()
{
    // about creator of programm
    QMessageBox msgBox(QMessageBox::Information,"Об авторе","Алексей Говорун\nФакультет ИУ\nКафедра ИУ8",QMessageBox::Ok);
    msgBox.exec();
}

void MainWindow::on_action_3_triggered()
{
    // new project

    QString str;
    str.setNum(projectNumber);

    QString s = QFileDialog::getSaveFileName(0,"Новый проект", "Project" + str, "*.govorun");

    if(s != "")
    {
            ui->plainTextEdit->setPlainText("");
            s = s + ".govorun";
            projectNumber++;
            QFile *f;
            f = new QFile(s);
            f->open(QFile::WriteOnly);
            f->close();
            nowProject = s;
            ui->nameProjectLabel->setText("Имя проекта:  " + s);
    }
}

void MainWindow::on_action_5_triggered()
{
    // save project

    if(nowProject != "")
    {
        QString s = ui->plainTextEdit->toPlainText();
        QFile * f = new QFile(nowProject);

        f->open(QFile::WriteOnly);
        f->close();

        f->open(QFile::Append);
        QTextStream out1(f);
        out1 << s;
        f->close();

        QMessageBox msgBox(QMessageBox::Information,"Сообщение","Сохранение файла прошло успешно.",QMessageBox::Ok);
        msgBox.exec();
    }
    else
    {
        QMessageBox msgBox(QMessageBox::Warning,"Ошибка","Имя проекта не было выбрано.",QMessageBox::Ok);
        msgBox.exec();
    }
}

void MainWindow::on_action_4_triggered()
{
    // open project

    QString s;
    s = QFileDialog::getOpenFileName(0,"Открыть проект", "", "*.govorun");

    if(s != "")
    {
        ui->plainTextEdit->setPlainText("");

        nowProject = s;

        QFile *f;
        f = new QFile(nowProject);

        f->open(QFile::ReadOnly);
        QTextStream in1(f);
        while(in1.atEnd() == false)
        {
            s = in1.readLine();
            ui->plainTextEdit->appendPlainText(s);
        }
        f->close();

        ui->nameProjectLabel->setText("Имя проекта:  " + nowProject);
    }
}

void MainWindow::on_action_6_triggered()
{
    // properties of text

    QFont fnt;
    bool ok;
    fnt = QFontDialog::getFont(&ok);
    if(ok == true) ui->plainTextEdit->setFont(fnt);
}


void MainWindow::on_action_XML_triggered()
{
    // import from XML

    QString s;
    s = QFileDialog::getOpenFileName(0,"Открыть XML файл", "", "*.xml");

    if(s != "")
    {
        int isOpenedTeg = 0;
        QString ans = "";

        ui->plainTextEdit->setPlainText("");

        QFile * f;
        f = new QFile(s);
        f->open(QFile::ReadOnly);

        QTextStream in1(f);
        while(in1.atEnd() == false)
        {
            s = in1.readLine();
            int n = s.length();

            for(int i = 0; i < n; ++i)
            {
                if(s[i] == '<')
                {
                    isOpenedTeg++;
                }
                else
                if(s[i] == '>')
                {
                    isOpenedTeg--;
                    ans += "  ";
                }
                else
                {
                    if(isOpenedTeg == 0)
                        if(s[i] != '\n' && s[i] != ' ' && s[i] != '\t')
                            ans += s[i];
                }
            }
        }

        ui->plainTextEdit->setPlainText(ans);

        f->close();
    }
}

void MainWindow::on_pushButton_clicked()
{
    close();
}

void MainWindow::on_action_8_triggered()
{
    ui->plainTextEdit->setPlainText("Алексей Говорун\nФакультет ИУ\nКафедра ИУ8");
}
