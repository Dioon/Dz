#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QMessageBox>
#include <QFileDialog>
#include "QFile"
#include "QDataStream"
#include <QTextStream>
#include <QFontDialog>
#include <QColorDialog>
#include <QDebug>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_action_2_triggered();

    void on_action_triggered();

    void on_action_3_triggered();

    void on_action_5_triggered();

    void on_action_4_triggered();

    void on_action_6_triggered();

    void on_action_XML_triggered();

    void on_pushButton_clicked();

    void on_action_8_triggered();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
